//
//  main.m
//  FirebaseAuth
//
//  Created by Msys on 02/01/17.
//  Copyright © 2017 Anand iOS Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
