//
//  AppDelegate.h
//  FirebaseAuth
//
//  Created by Msys on 02/01/17.
//  Copyright © 2017 Anand iOS Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;
@import GoogleSignIn;

@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate,GIDSignInUIDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

