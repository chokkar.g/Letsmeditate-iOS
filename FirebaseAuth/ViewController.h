//
//  ViewController.h
//  FirebaseAuth
//
//  Created by Msys on 02/01/17.
//  Copyright © 2017 Anand iOS Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;
@import GoogleSignIn;

@interface ViewController : UIViewController<GIDSignInUIDelegate,GIDSignInDelegate>

@property(weak, nonatomic) IBOutlet GIDSignInButton *signInButton;

- (IBAction)SignInButton:(id)sender;

@end

